require('dotenv') .config();
require('./src/config/database').connect();
const cors = require('cors');

const authController = require('./src/auth/auth.controller');
const userController = require('./src/user/user.controller');
const chattingController = require('./src/chatting/chatting.controller');
const responseDto = require('./src/common/dto/response.dto')
const express = require('express');


const app = express();
const router = express.Router();

// Allow all traffics
app.use(cors());

// Parse body to JSON
app.use(express.json());

// Use customize response 
app.use(responseDto);

router.use('/auth', authController);
router.use('/user', userController);
router.use('/conversation', chattingController)

// Set prefix routers
app.use('/api/v1', router);

module.exports = app;