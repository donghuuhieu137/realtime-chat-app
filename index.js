// Import Cache configuration
require ('./src/config/cache').redisClientConfig();

const http = require("http");
const app = require("./app");
const server = http.createServer(app);
const io = require('socket.io')(server)

const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

// WebSocket import
module.exports = io;
require('./src/websocket/websocket.gateway').handler();

// Server listening 
server.listen(port, (err) => {
  if (err)
    console.log(`Something went wrong !!\nError: ${err}`);
  console.log(`Server running on port ${port}`);
});



