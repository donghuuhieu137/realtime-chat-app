[![pipeline status](https://gitlab.com/donghuuhieu137/realtime-chat-app/badges/main/pipeline.svg)](https://gitlab.com/donghuuhieu137/realtime-chat-app/-/commits/main)

# Realtime-Chat-App

## API Documents
[API_DOCUMENT_LINK](https://documenter.getpostman.com/view/14673104/UVXetdxi)

## Requirements

- [NodeJS v16.13.2](https://nodejs.org/en/)
- [MongoDB v5.0](https://www.mongodb.com/)
- [Redis v6.2.6](https://redis.io/)


## Installation
Rename file `example.env` to `.env`

Run command `npm run start:prod`

### Stay in touch
- Author: [Hieu Dong Huu](https://www.facebook.com/donghuuhieu137/)
- Email: [donghuuhieu137@gmail.com](mail:donghuuhieu137@gmail.com)
- Linkedin: [@donghuuhieu137](https://www.linkedin.com/in/donghuuhieu137/)
