FROM node:16.13.2

COPY ["package.json", "app.js", "index.js", "/app/"]

WORKDIR /app

RUN ls -al
RUN npm install

COPY ["src", "/app/src"]

CMD ["npm", "run", "start:prod"]
