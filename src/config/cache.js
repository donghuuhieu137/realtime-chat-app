// Redis import
const redis = require('redis');
const {REDIS_HOST, REDIS_PORT} = process.env;
const redisClient = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT
});

const redisClientConfig = async () =>
  await redisClient.connect();

module.exports = {
  redisClient,
  redisClientConfig
}