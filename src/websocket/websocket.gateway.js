const chattingService = require('../chatting/chatting.service');
const authService = require('../auth/auth.service');
const User = require('../user/user.entity');
const io = require('../../index');
const {removeCache} = require('../common/helpers/cache.helper');


const handler = async() => {
  io.on('connect', async (client) => {
    
    console.log('Client connected...');
    
    client.on('login', async (payload) => {
      const session = await authService.validateToken(payload?.refreshToken);
      const user = await User.findById(session?.user?._id);
      user.is_active = true;
      removeCache(`user?userId=${user._id.valueOf()}`);
      await user.save();
      client.join(session?.user?._id.valueOf());
      console.log(session?.user?._id.valueOf());
      client.user = session?.user?.id.valueOf();
      client.emit('login.result', 'Connected');
    });

    client.on('messages', async (payload) => {
      if(client?.user){
        const {conversationId, senderId, messageType, content} = payload;
        const message = await chattingService.createMessage(conversationId, senderId, messageType, content);
        const conversation = await chattingService.getConversationById(conversationId);
        conversation.updatedAt = new Date();
        await conversation.save();
        io
          .to(`${conversation?.l_user}`)
          .to(`${conversation?.r_user}`)
          .emit('messages', message);
      }
    });

    client.on('logout', async (payload) =>{
      const userId = payload?.userId
      const user = await User.findById(userId);
      user.is_active = false;
      await user.save();
      removeCache(`user?userId=${user._id.valueOf()}`);
      io.emit('logout.result', 'Disconnected');
    });
  })
}

module.exports = {
  handler
}