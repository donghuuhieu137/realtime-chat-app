const {redisClient} = require('../../config/cache')

const {CACHE_EXPIRATION_SECONDS} = process.env;

const getOrSetCache = async (key, cb) =>{
  const result = await redisClient.get(key);
  if(!result){
    const value = await cb();
    const setResult = await redisClient.setEx( key, CACHE_EXPIRATION_SECONDS, JSON.stringify(value) );
    if(setResult !== 'OK')
      throw new Error('Cannot set cache key !!!');
    return value;
  }
  return JSON.parse(result);
}

const removeCache = async (key) => {
  const result = await redisClient.del(key);
}

// const getOrSetCache = async (key, cb) =>{
//   return new Promise((resolve, reject) => {
//     redisClient.get(key, async (error, data) => {
//       if(error)
//         return reject(error);
//       if(data)
//         return resolve(JSON.parse(data));
//       const freshData = await cb();
//       console.log(freshData);
//       redisClient.setEx(key, CACHE_EXPIRATION_MINUTES, JSON.stringify(freshData));
//       resolve(freshData);
//     })
//   })
// }

module.exports = {
  getOrSetCache,
  removeCache
}