const jwt = require('jsonwebtoken');
const { JWT_SECRET } = process.env;


const getCurrentUser = (req) => {
  const token = req.headers.authorization.split(' ')[1];
  const decodedToken = jwt.verify(token, JWT_SECRET);
  const userId = decodedToken.userId;
  return userId;
}

module.exports = {
  getCurrentUser
}