const responseDto = (req, res, next) => {

  /**
   * (default status 200)
   * Success response
   */
  res.success = function (data, total, meta ) {
    return res.json({
      message: 'Success',
      code: 200,
      data,
      total,
      meta,
      time: new Date()
    })
  }

  /**
   * Custom error response
   */
  res.error = function(err, meta) {
    if (typeof err === 'string') {
      return res.json({
        message: `${err}`,
        meta: meta,
        time: new Date(),
      });
    } else {
      return res.json({
        message: `${err?.message}`,
        errorCode: err?.code,
        meta: meta,
        time: new Date(),
      });
    }
  }

  /**
   * (status 403)
   * Bad request response
   */
  res.badrequest = function(message="Bad request", code=400) {
    return res.status(400).error({ message, code })
  }

  /**
   * (status 403)
   * Forbidden request response
   */
  res.forbidden = function(message="Forbidden", code=403) {
    return res.status(403).error({ message, code })
  }
  
  /**
   * (status 401)
   * Unauthorize request response
   */
  res.unauthorize = function(message="Unauthorize", code=401) {
    return res.status(403).error({ message, code })
  }

  /**
   * (status 500)
   * Internal request response
   */
  res.internal = function(message="Internal error", code=500) {
    return res.status(500).error({ message, code })
  }

  next()
}

module.exports = responseDto;