const express = require('express');
const User = require('./user.entity');
const router = express.Router();
const helper = require('../common/helpers/current-user')
const {getOrSetCache} = require('../common/helpers/cache.helper');
const jwtAuthGuard = require('../common/guards/jwt-auth.guards')

router.get('/', jwtAuthGuard , async (req, res) => {
  const userId = await helper.getCurrentUser(req);
  const user = await getOrSetCache(`user?userId=${userId}`, async () => {
    const userInDb = await User.findById(userId);
    return userInDb;
  })
  res.success(user);
});

module.exports = router;