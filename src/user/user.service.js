const User = require("./user.entity");
const bcrypt = require('bcryptjs');

module.exports = {
  async createUser(userBody) {
    console.log(userBody);
    const { name, email, password } = userBody;
    console.log('Save user');
    const encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
        name,
        email: email.toLowerCase(),
        password: encryptedPassword,
        is_active: false,
      });
    return user;
  },

  async findByEmail(email) {
      console.log("Find by name or email");
      return await User.findOne({
          email
      });
  },

  async checkEmailIsValid(email) {
      console.log('Check email');
      return !!(await User.findOne({email}));
  }

}