const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const toJSON = require('../common/plugins/toJSON');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        private: true
    },
    is_active: {
        type: Boolean,
        required: false,
        default: false
    }, 
  },
  {
    timestamps: true
  },
  { 
    versionKey: false 
  }
);

userSchema.methods.comparePassword = async function (password){
  return bcrypt.compare(password, this.password);
};

userSchema.plugin(toJSON);

const User = mongoose.model('user', userSchema);

module.exports = User;