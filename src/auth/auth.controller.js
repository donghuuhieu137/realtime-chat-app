const express = require('express');
const router = express.Router();
const authService = require('./auth.service');
const userService = require('../user/user.service');

router.get('/', (req, res) => {
  return res.badreq();
});

router.post('/login', async (req, res) => {
  const {email, password} = req.body;
  const user = await authService.login(email, password);
  const token = await authService.generateAuthToken(user);
  res.success({ user, token });
});

router.post('/register', async (req, res) => {
  try {
    // Get request data
    const { name, email, password } = req.body;
    if (!(email && password && name))
      res.badrequest("Invalid input !!!");
    if (await userService.checkEmailIsValid(email))
      return res.badrequest("Email already exist !!");
    const user = await userService.createUser(req.body);
    res.success(user);
  } catch (err) {
    console.log(err);
    res.error(err);
  }
});

router.post('/logout', async (req, res) => {
  try {
    const {refreshToken} = req.body;
    await authService.logout(refreshToken);
    return res.success();
  } catch (err) {
    console.log(err);
    res.error(err);
  }
});

router.post('/refresh-tokens', async (req, res) => {
  try {
    const {refreshToken} = req.body;
    const newToken = await authService.refreshAuth(refreshToken);
    return res.success(newToken);
  } catch (err) {
    console.log(err);
    res.error(err);
  }
});

module.exports = router;