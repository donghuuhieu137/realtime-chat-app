const res = require('express/lib/response');
const userService = require('../user/user.service');
const jwt = require('jsonwebtoken');
const User = require('../user/user.entity');
const tokenTypes = require('../common/constants/token-type');
const Token = require('../auth/token.entity');
const moment = require('moment');
const tokenType = require('../common/constants/token-type');
const { JWT_SECRET, JWT_ACCESS_EXPIRATION_MINUTES, JWT_REFRESH_EXPIRATION_DAYS } = process.env;


const login = async (email, password) => {
  const user = await userService.findByEmail(email);
  console.log(user);
  if (!user || !(await user.comparePassword(password)))
    return res.unauthorize();
  return user;
};

const generateToken = async (userId, expires, type) => {
  const payload = {
    userId: userId,
    iat: moment().unix(),
    exp: expires.unix(),
    type
  }
  return await jwt.sign(payload, JWT_SECRET);
};

const saveToken = async (token, userId, expires, type) => {
  const createdToken = await Token.create({
      token,
      user: userId,
      expires: expires.toDate(),
      type
  })
  return createdToken;
};

const generateAuthToken = async (user) =>{
  const accessTokenExpires = moment().add(JWT_ACCESS_EXPIRATION_MINUTES, 'minutes');
  const accessToken = await generateToken(user._id, accessTokenExpires, tokenTypes.ACCESS);

  const refreshTokenExpires = moment().add(JWT_REFRESH_EXPIRATION_DAYS, 'days');
  const refreshToken = await generateToken(user._id, refreshTokenExpires, tokenTypes.REFRESH);

  await saveToken(refreshToken, user._id, refreshTokenExpires, tokenTypes.REFRESH);
  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toDate(),
    },
  };
};

const refreshAuth = async (refreshToken) =>{
  const token = await Token.findOne({token: refreshToken, type: tokenTypes.REFRESH});
  if(!token)
    throw new Error('refresh token invalid !!!');
  const user = await User.findById(token.user);
  if(!user)
    throw new Error('User in refresh token not found !!!');
  await token.remove();
  const newToken = await generateAuthToken(user);
  return newToken;
};

const logout = async (refreshToken) => {
  const token = await Token.findOne({token: refreshToken, type: tokenTypes.REFRESH});
  if(!token)
    throw new Error('Token is undefined !!!');
  await token.remove();
};

const validateToken = async (refreshToken) =>{
  const tokenInDb = await Token.findOne({
    token: refreshToken,
    type: tokenTypes.REFRESH
  })
  if(!tokenInDb || tokenInDb.expires < new Date())
    throw new Error('Token is invalid !!!');
  return tokenInDb;
} 

module.exports = {
  login,
  logout,
  generateAuthToken,
  refreshAuth,
  logout,
  validateToken
}