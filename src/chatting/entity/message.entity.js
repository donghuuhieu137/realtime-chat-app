const mongoose = require('mongoose');
const toJSON = require('../../common/plugins/toJSON');

const messageSchema = new mongoose.Schema({
    conversation_id: {
      type: String,
      required: true
    },
    sender_id: {
      type: String,
      required: true
    },
    message_type:{
      type: String
    },
    content: {
      type: String
    }
  },
  {
    timestamps: true
  },
  { 
    versionKey: false 
  }
);

messageSchema.plugin(toJSON);

const Message = mongoose.model('message', messageSchema);

module.exports = Message;