const mongoose = require('mongoose');
const toJSON = require('../../common/plugins/toJSON');

const conversationSchema = new mongoose.Schema({
    l_user: {
      type: String,
      required: true
    },
    r_user: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  },
  { 
    versionKey: false 
  }
);

conversationSchema.plugin(toJSON);

const Conversation = mongoose.model('conversation', conversationSchema);

module.exports = Conversation;