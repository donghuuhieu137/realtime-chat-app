const messageType = {
  TEXT: 'text',
  IMAGE: 'image'
}

module.exports = messageType