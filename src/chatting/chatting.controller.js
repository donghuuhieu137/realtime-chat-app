const express = require('express');
const router = express.Router();
const jwtAuthGuard = require('../common/guards/jwt-auth.guards')
const chattingService = require('./chatting.service')
const helper = require('../common/helpers/current-user')
const { messageType } = require('./constant/message-type')

router.get('/', jwtAuthGuard , async (req, res) => {
  const {limit, offset} = req?.query;
  const userId = helper.getCurrentUser(req);
  const conversations = await chattingService.getConversations(userId,limit,offset);
  await res.json(conversations);
});

router.get('/:id', jwtAuthGuard , async (req, res) => {
  const {limit, offset} = req?.query;
  const conversationId = req.params.id;
  const messages = await chattingService.getMessages(conversationId,limit,offset);
  await res.json(messages);
});

router.post('/', jwtAuthGuard , async (req, res) => {
  const {l_user, r_user} = req.body;
  const conversation = await chattingService.createConversation(l_user, r_user);
  await res.json(conversation);
});

router.post('/message', jwtAuthGuard , async (req, res) => {
  const {conversation_id, sender_id, message_type, content} = req.body;
  const message = await chattingService.createMessage(conversation_id, sender_id, message_type, content);
  await res.json(message);
});


module.exports = router;