const Conversation = require('./entity/conversation.entity')
const Message = require('./entity/message.entity')

const getConversations = async (userId, limit, offset) => {
  const conversations = Conversation.find({
    $or: [{ l_user: userId }, { r_user: userId }],
  })
    .sort({ updatedAt: -1 })
    .limit(parseInt(limit))
    .skip(parseInt(offset));
  return conversations;
}

const getConversationById = async (conversationId) => {
  const conversation = Conversation.findById(conversationId)
  return conversation;
}

const createConversation = async (l_user, r_user) => {
  const conversationInDb = Conversation.find({
    l_user,
    r_user,
  });
  if (conversationInDb) {
    console.log("Already have conversation between these two")
    return conversationInDb;
  }
  const conversation = Conversation.create({
    l_user,
    r_user,
  })
  return conversation;
}

const getMessages = async (conversationId, limit, offset) => {
  const messages = Message.find({
    conversation_id: conversationId
  })
    .sort({ createdAt: -1 })
    .limit(parseInt(limit))
    .skip(parseInt(offset));
  return messages;
}

const createMessage = async (conversationId, senderId, messageType, content) => {
  const message = await Message.create({
    conversation_id: conversationId,
    sender_id: senderId,
    message_type: messageType,
    content
  })
  return message;
}

module.exports = {
  getConversations,
  getConversationById,
  createConversation,
  getMessages,
  createMessage
}